package com.binary_studio.academy_coin;

import java.util.stream.Stream;

public final class AcademyCoin {

	private AcademyCoin() {
	}

	public static int maxProfit(Stream<Integer> prices) {
		Integer[] arrayPrices = prices.toArray(Integer[]::new);
		int sum = 0;
		for (int i = 0; i < arrayPrices.length - 1; i++) {
			if (arrayPrices[i] < arrayPrices[i + 1]) {
				int currentPrices = arrayPrices[i + 1];
				int index = i + 2;

				while (index < arrayPrices.length && currentPrices <= arrayPrices[index]) {
					currentPrices = arrayPrices[index];
					index++;
				}

				sum += currentPrices - arrayPrices[i];
				i = index - 1;
			}
		}
		return sum;
	}

}
