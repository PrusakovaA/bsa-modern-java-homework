package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {

	private String name;

	private PositiveInteger powergridRequirments;

	private PositiveInteger capacitorConsumption;

	private PositiveInteger optimalSpeed;

	private PositiveInteger optimalSize;

	private PositiveInteger baseDamage;

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {
		AttackSubsystemImpl attackSubsystem = new AttackSubsystemImpl();
		if (name == null || name.trim().isEmpty()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		attackSubsystem.name = name;
		attackSubsystem.powergridRequirments = powergridRequirments;
		attackSubsystem.capacitorConsumption = capacitorConsumption;
		attackSubsystem.optimalSize = optimalSize;
		attackSubsystem.optimalSpeed = optimalSpeed;
		attackSubsystem.baseDamage = baseDamage;
		return attackSubsystem;
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powergridRequirments;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	@Override
	public PositiveInteger attack(Attackable target) {
		int targetSize = target.getSize().value();
		int optimalSize = this.optimalSize.value();
		double sizeReductionModifier = targetSize >= optimalSize ? 1 : (double) targetSize / optimalSize;
		int targetSpeed = target.getCurrentSpeed().value();
		int optimalSpeed = this.optimalSpeed.value();
		double speedReductionModifier = targetSpeed <= optimalSpeed ? 1 : (double) optimalSpeed / (2 * targetSpeed);
		int damage = (int) Math.ceil(this.baseDamage.value() * Math.min(sizeReductionModifier, speedReductionModifier));
		return PositiveInteger.of(damage);
	}

	@Override
	public String getName() {
		return this.name;
	}

}
