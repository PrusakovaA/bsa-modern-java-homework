package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private String name;

	private PositiveInteger currentSpeed;

	private PositiveInteger size;

	private PositiveInteger powerGrid;

	private PositiveInteger capacitor;

	private PositiveInteger capacitorRechargeRate;

	private PositiveInteger hull;

	private PositiveInteger shield;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	private final PositiveInteger initialCapacitor;

	public CombatReadyShip(String name, PositiveInteger velocity, PositiveInteger size, PositiveInteger powerGrid,
			PositiveInteger capacitor, PositiveInteger capacitorRecharge, PositiveInteger hull, PositiveInteger shield,
			AttackSubsystem attackSubsystem, DefenciveSubsystem defenciveSubsystem, PositiveInteger initialCapacitor) {
		this.name = name;
		this.currentSpeed = velocity;
		this.size = size;
		this.powerGrid = powerGrid;
		this.capacitor = capacitor;
		this.capacitorRechargeRate = capacitorRecharge;
		this.hull = hull;
		this.shield = shield;
		this.attackSubsystem = attackSubsystem;
		this.defenciveSubsystem = defenciveSubsystem;
		this.initialCapacitor = initialCapacitor;
	}

	@Override
	public void endTurn() {
		PositiveInteger capacitorAmount = PositiveInteger
				.of(this.capacitor.value() + this.capacitorRechargeRate.value());
		this.capacitor = PositiveInteger.of(capacitorAmount.value() > this.initialCapacitor.value()
				? this.initialCapacitor.value() - this.capacitor.value() : capacitorAmount.value());
	}

	@Override
	public void startTurn() {
		// TODO: Ваш код здесь :)

	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.currentSpeed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		if (this.capacitor.value() < this.attackSubsystem.getCapacitorConsumption().value()) {
			return Optional.empty();
		}
		else {
			this.capacitor = PositiveInteger
					.of(this.capacitor.value() - this.attackSubsystem.getCapacitorConsumption().value());
			PositiveInteger damage = this.attackSubsystem.attack(target);
			AttackAction attackAction = new AttackAction(damage, this, target, this.attackSubsystem);
			return Optional.of(attackAction);
		}
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		AttackAction attackAction = this.defenciveSubsystem.reduceDamage(attack);
		if (attackAction.damage.value() < this.shield.value()) {
			this.shield = PositiveInteger.of(this.shield.value() - attack.damage.value());
			return new AttackResult.DamageRecived(attack.weapon, attackAction.damage, this);
		}
		else {
			PositiveInteger demage = PositiveInteger.of(attackAction.damage.value() - this.shield.value());
			if (demage.value() < this.hull.value()) {
				this.hull = PositiveInteger.of(this.hull.value() - demage.value());
				return new AttackResult.DamageRecived(attack.weapon, attackAction.damage, this);
			}
		}
		return null;
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		// TODO: Ваш код здесь :)
		return null;
	}

}
