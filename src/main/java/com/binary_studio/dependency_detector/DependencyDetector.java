package com.binary_studio.dependency_detector;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public final class DependencyDetector {

	private DependencyDetector() {
	}

	public static boolean canBuild(DependencyList libraries) {
		if (libraries.libraries.isEmpty()) {
			return true;
		}
		int numOfLibs = libraries.libraries.size();
		List<List<Integer>> adj = new ArrayList<>(numOfLibs);

		for (int i = 0; i < numOfLibs; i++) {
			adj.add(new LinkedList<>());
		}

		for (String[] dependency : libraries.dependencies) {
			int source = libraries.libraries.indexOf(dependency[0]);
			int dest = libraries.libraries.indexOf(dependency[1]);
			adj.get(source).add(dest);
		}

		boolean[] visited = new boolean[numOfLibs];
		boolean[] recStack = new boolean[numOfLibs];

		for (int i = 0; i < numOfLibs; i++) {
			if (isCycles(i, visited, recStack, adj)) {
				return false;
			}
		}

		return true;
	}

	private static boolean isCycles(int index, boolean[] visited, boolean[] recStack,
			List<List<Integer>> dependencies) {
		if (recStack[index]) {
			return true;
		}
		if (visited[index]) {
			return false;
		}

		visited[index] = true;
		recStack[index] = true;

		List<Integer> children = dependencies.get(index);

		for (Integer c : children) {
			if (isCycles(c, visited, recStack, dependencies)) {
				return true;
			}
		}

		recStack[index] = false;

		return false;
	}

}
